package com.example.timer;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private int minutes = 0, seconds = 0;
    private EditText editTextMinutes, editTextSeconds;
    private TextView timeLabel;
    private ImageButton buttonRestart;
    private ToggleButton buttonStartStop;
    private ImageView imageViewSpinner;

    private CountDownTimer timer;
    private AnimationDrawable frameAnimation;

    private boolean isStart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextMinutes = (EditText)findViewById(R.id.editTextMin);
        editTextSeconds = (EditText)findViewById(R.id.editTextSec);
        timeLabel = (TextView)findViewById(R.id.textViewTime);
        buttonRestart = (ImageButton)findViewById(R.id.buttonRestart);
        buttonStartStop = (ToggleButton)findViewById(R.id.toggleButtonStartStop);
        imageViewSpinner = (ImageView)findViewById(R.id.imageViewSpinner);

        editTextMinutes.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence text, int start, int count, int after) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isStart)
                {
                    try {
                        minutes = Integer.parseInt(String.valueOf(editTextMinutes.getText()));
                        if (minutes < 0) minutes = 0;
                        updateTimeLabel();
                    }
                    catch (Exception e)
                    {
                        minutes = 0;
                        if (String.valueOf(editTextMinutes.getText()).length() != 0)
                            Toast.makeText(MainActivity.this, "Введено не корректное значение минут", Toast.LENGTH_LONG).show();
                        updateTimeLabel();
                    }
                }
            }
        });

        editTextSeconds.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence text, int start, int count, int after) {
                if (!isStart)
                {
                    try {
                        seconds = Integer.parseInt(String.valueOf(editTextSeconds.getText()));
                        if (seconds > 60) seconds = 60;
                        if (seconds < 0) seconds = 0;
                        updateTimeLabel();
                    }
                    catch (Exception e)
                    {
                        seconds = 0;
                        if (String.valueOf(editTextSeconds.getText()).length() != 0)
                            Toast.makeText(MainActivity.this, "Введено не корректное значение секунд", Toast.LENGTH_LONG).show();
                        updateTimeLabel();
                    }
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,int after) { }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStartStop.setChecked(false);
                timer.cancel();
                restart();
                pauseSpinner();
                clearSpinner();
            }
        });

        buttonStartStop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    int secTimer = minutes * 60 + seconds;
                    if (secTimer > 0)
                        startTimer();
                    else
                    {
                        buttonView.setChecked(false);
                        Toast.makeText(MainActivity.this, "Введите время отличное от 0", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    if (timer != null)
                    {
                        timer.cancel();
                        pauseSpinner();
                        if (!isStart) restart();
                    }
                }
            }
        });
    }

    private void startTimer()
    {
        isStart = true;
        int secTimer = minutes * 60 + seconds;
        secTimer *= 1000;
        timer = new CountDownTimer(secTimer, 1000) {

            public void onTick(long millisUntilFinished) {
                downSecond(1);
                updateTimeLabel();
            }

            public void onFinish()
            {
                isStart = false;
                timeLabel.setText(getString(R.string.done));
                playNotification();
                pauseSpinner();
                clearSpinner();
                buttonStartStop.setChecked(false);
            }
        };
        timer.start();
        startSpinner();
    }

    public void downSecond (int count)
    {
        int secTimer = minutes * 60 + seconds;
        secTimer -= count;
        minutes = secTimer / 60;
        seconds = secTimer % 60;
    }

    private void restart () {
        try {
            minutes = Integer.parseInt(String.valueOf(editTextMinutes.getText()));
            if (minutes < 0) minutes = 0;
            updateTimeLabel();
        }
        catch (Exception e)
        {
            minutes = 0;
            updateTimeLabel();
        }

        try {
            seconds = Integer.parseInt(String.valueOf(editTextSeconds.getText()));
            if (seconds > 60) seconds = 60;
            if (seconds < 0) seconds = 0;
            updateTimeLabel();
        }
        catch (Exception e)
        {
            seconds = 0;
            updateTimeLabel();
        }
    }

    private void updateTimeLabel()
    {
        String sec = Integer.toString(seconds), min = Integer.toString(minutes);
        if (Integer.toString(seconds).length() == 1)
        {
            sec = "0" + Integer.toString(seconds);
        }
        if (Integer.toString(minutes).length() == 1)
        {
            min = "0" + Integer.toString(minutes);
        }
        String time = min + ":" + sec;
        timeLabel.setText(time);
    }

    private void playNotification ()
    {
        MediaPlayer mPlayer = MediaPlayer.create(this, R.raw.notification);
        mPlayer.start();
    }

    private void startSpinner()
    {
        imageViewSpinner.setBackgroundResource(R.drawable.spinner);
        // получаем объект анимации
        frameAnimation = (AnimationDrawable) imageViewSpinner.getBackground();
        // по нажатию на ImageView
        frameAnimation.start();
    }

    private void pauseSpinner()
    {
        frameAnimation.stop();
    }

    private void clearSpinner()
    {
        imageViewSpinner.setBackgroundColor(Color.alpha(1));
    }

}