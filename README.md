# Лабораторная работа 5. Таймер
Пользователь задает количество минут и секунд для обратного отсчета, может 
запускать/останавливать (Toogle button) и сбрасывать таймер. Каждую секунду на 
экране должно изменяться количество оставшегося времени и проигрываться анимация 
(в Image View). По истечению времени должен быть воспроизведен звук.

Приложение следующего вида:

![Дизайн приложения](doc/timer_design.jpg)

Cсылки для выполнения работы:

Элемент Toogle button https://developer.android.com/guide/topics/ui/controls/togglebutton

Покадровая анимация https://metanit.com/java/android/19.1.php

Работа с аудиофайлами https://metanit.com/java/android/11.2.php

Классы Timer и TimerTask http://developer.alexanderklimov.ru/android/java/timer.php, https://developer.android.com/reference/java/util/TimerTask?hl=en

Таймер обратного отсчета CountDownTimer 

https://developer.android.com/reference/android/os/CountDownTimer

Видео работы таймера https://www.youtube.com/watch?v=MDuGwI6P-X8